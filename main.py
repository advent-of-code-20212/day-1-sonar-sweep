def main():
    largerCounter = 0

    with open('inputs.txt', 'r') as f:
        lines = f.readlines()
        lines = [int(line) for line in lines]
        averagedLines = [] 

        # making average numbers
        for i in range(len(lines)-2): 
            averagedLines.append(sum(lines[i:i+3]))

        for i in range(1, len(averagedLines)): 
            if averagedLines[i-1] < averagedLines[i]: 
                largerCounter += 1
    
    print(largerCounter)

if __name__ == "__main__":
    main()